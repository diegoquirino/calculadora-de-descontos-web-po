package net.diegoquirino.calculator.ux.pages;

public class Locators {

    static final String CSS_MENU_PRODUCT = "a.nav-link[href='/produtos']";

    static final String ID_SELECT_CLIENT_TYPE = "tipoCliente";
    static final String ID_INPUT_QUANTITY = "quantidade";
    static final String ID_INPUT_DISCOUNT_FACTOR_RESULT = "fator";
    static final String XPATH_TABLE_ITEM_PRODUCT_LIST = "//a[contains(@href, '/itens/add/produto/";

}
